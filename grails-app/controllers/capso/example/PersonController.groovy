package capso.example

import grails.converters.JSON

class PersonController {

	def personService
	
    def index() {
		def r = personService.findAll()
		render r as JSON
	}
	
}
