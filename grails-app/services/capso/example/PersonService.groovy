package capso.example

import capso.example.Person;
import grails.transaction.Transactional


class PersonService {

	static transactional = true
 	
	
    def findAll() {
		Person.all
		//Person.createCriteria().list() {}//{ gt('age', 5) }
		//Person.executeQuery("select p from Person p") // will cause unit tests to fail as the HQLs are not supported for mocked domain classes, works only in the integration level
	}

}
