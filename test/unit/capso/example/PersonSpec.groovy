package capso.example

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Person)
class PersonSpec extends Specification {

	def setup() {
	}

	def cleanup() {
	}

	void "test Person validation"() {
		given: // or setup:
		def p = new Person(name: name, age: age)

		when:
		def v = p.validate()

		then:
		v == validates
		errors.every { p.errors[it] != null }

		where:
		name 	| age 	| validates | errors
		'' 		| -3	| false		| ['age', 'name']
		'' 		| 22	| false		| ['name']
		'John' 	| -1	| false		| ['age']
		'Anne' 	| 34	| true		| []
	}
}
