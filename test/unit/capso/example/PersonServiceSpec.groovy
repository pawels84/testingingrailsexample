package capso.example

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Ignore
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(PersonService)
@Mock(Person)
class PersonServiceSpec extends Specification {

	def setup() {
	}

	def cleanup() {
	}

	void "test findAll returns empty list if there are no Person instances in the database"() {
		expect:
		service.findAll().empty
	}

	void "test findAll returns all Person instances present in the database"() {
		given:
		//Person.metaClass.static.executeQuery = { String query -> persons } // it's a hack, we should not really do that because the query itself is part of the service's implementation and we definitely want to test if it returns valid result set
		persons.each { it.save() }

		expect:
		service.findAll() as Set == persons as Set

		where:
		persons = [
			new Person(name: 'John', age: 31),
			new Person(name: 'Lucy', age: 24)
		]
	}
}
