package capso.example

import grails.converters.JSON
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(PersonController)
@Mock(Person)
class PersonControllerSpec extends Specification {

	def setup() {
	}

	def cleanup() {
	}

	/**
	 * a pure unit test, having mocked the PersonService collaborator, testing only the controller behavior
	 */
	void "test index returns JSON with Persons provided by PersonService.findAll"() {
		given:
		persons.each { it.save() }
		controller.personService = Mock(PersonService)
		controller.personService.findAll() >> persons

		when:
		controller.index()

		then:
		response.json == JSON.parse((persons as JSON).toString())

		where:
		persons = [
			new Person(name: 'Alice', age: 3),
			new Person(name: 'Joe', age: 23),
			new Person(name: 'Mary', age: 44)
		]
	}

	/**
	 * not a pure unit test using the PersonService, hence when the service has a bug the test will fail even if the controller implementation is valid
	 * it's like an integration test run as a unit test
	 */
	void "test index returns JSON with all Persons present in the database"() {
		given:
		persons.each { it.save() }
		controller.personService = new PersonService() // controller is going to use the real thing, the test will fail in case something is wrong with the service

		when:
		controller.index()

		then:
		response.json == JSON.parse((persons as JSON).toString())

		where:
		persons = [
			new Person(name: 'Luke', age: 3),
			new Person(name: 'Jack', age: 23),
			new Person(name: 'Lucy', age: 54)
		]
	}
}
