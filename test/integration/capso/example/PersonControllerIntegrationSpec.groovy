package capso.example

import grails.converters.JSON
import grails.test.spock.IntegrationSpec
import spock.lang.*

/**
 *
 */
class PersonControllerIntegrationSpec extends IntegrationSpec {

	def controller = applicationContext.getBean(PersonController)

	def setup() {
	}

	def cleanup() {
	}

	void "test index returns JSON with all Persons present in the database"() {
		given:
		persons.each { it.save() }

		when:
		controller.index()

		then:
		controller.response.json == JSON.parse((persons as JSON).toString())

		where:
		persons = [
			new Person(name: 'Luke', age: 3),
			new Person(name: 'Jack', age: 23),
			new Person(name: 'Lucy', age: 54)
		]
	}
}
