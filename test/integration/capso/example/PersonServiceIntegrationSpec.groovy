package capso.example

import grails.test.spock.IntegrationSpec
import spock.lang.*

/**
 *
 */
class PersonServiceIntegrationSpec extends IntegrationSpec {

	def service = applicationContext.getBean(PersonService)

	def setup() {
	}

	def cleanup() {
	}

	void "test findAll returns empty list if there are no Person instances in the database"() {
		expect:
		service.findAll().empty
	}

	void "test findAll returns all Person instances present in the database"() {
		given:
		persons.each { it.save() }

		expect:
		service.findAll() as Set == persons as Set

		where:
		persons = [
			new Person(name: 'John', age: 31),
			new Person(name: 'Lucy', age: 24)
		]
	}
}
